import { Component, OnInit } from '@angular/core';
import { Article } from '../interfaces/interfaces';
import { NoticiasService } from '../services/noticias.service';

@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.page.html',
  styleUrls: ['./noticias.page.scss'],
})
export class NoticiasPage implements OnInit {

  noticias: Article[] =[];

  constructor( private noticiasServices: NoticiasService) { }

  ngOnInit() {

    this.noticiasServices.getTopHeadlines()
    .subscribe(  resp =>   {
      console.log('noticias', resp );
      //this.noticias = resp.articles;
      this.noticias.push( ...resp.articles);
    });
  }

}
