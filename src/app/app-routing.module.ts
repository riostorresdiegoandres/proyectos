import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home/Inbox',
    pathMatch: 'full'
  },
  {
    path: 'inicio',
    loadChildren: () => import('./inicio/inicio.module').then( m => m.InicioPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'noticias',
    loadChildren: () => import('./noticias/noticias.module').then( m => m.NoticiasPageModule)
  },
  {
    path: 'eventos',
    loadChildren: () => import('./eventos/eventos.module').then( m => m.EventosPageModule)
  },
  {
    path: 'cuenta',
    loadChildren: () => import('./cuenta/cuenta.module').then( m => m.CuentaPageModule)
  },
  {
    path: 'noticiasnuevas',
    loadChildren: () => import('./noticiasnuevas/noticiasnuevas.module').then( m => m.NoticiasnuevasPageModule)
  },
  {
    path: 'eventosnuevos',
    loadChildren: () => import('./eventosnuevos/eventosnuevos.module').then( m => m.EventosnuevosPageModule)
  },
  {
    path: 'programas',
    loadChildren: () => import('./programas/programas.module').then( m => m.ProgramasPageModule)
  },
  {
    path: 'forgot',
    loadChildren: () => import('./forgot/forgot.module').then( m => m.ForgotPageModule)
  },
  {
    path: 'conoce',
    loadChildren: () => import('./conoce/conoce.module').then( m => m.ConocePageModule)
  },
  {
    path: 'aplicativo',
    loadChildren: () => import('./aplicativo/aplicativo.module').then( m => m.AplicativoPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
