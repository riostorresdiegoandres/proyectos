import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NoticiasnuevasPageRoutingModule } from './noticiasnuevas-routing.module';

import { NoticiasnuevasPage } from './noticiasnuevas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NoticiasnuevasPageRoutingModule
  ],
  declarations: [NoticiasnuevasPage]
})
export class NoticiasnuevasPageModule {}
