import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NoticiasnuevasPage } from './noticiasnuevas.page';

const routes: Routes = [
  {
    path: '',
    component: NoticiasnuevasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NoticiasnuevasPageRoutingModule {}
