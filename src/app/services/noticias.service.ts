import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { RespuestaTopHeadlines } from '../interfaces/interfaces';

const apikey = environment.apikey;
const apiUlr = environment.apiUlr;

const headers = new HttpHeaders({
  'X-API-KEY': apikey
});

@Injectable({
  providedIn: 'root'
})
export class NoticiasService {

  constructor( private http: HttpClient) { }


    private ejecutarQuery<T>( query: string){

      query = apiUlr + query;

      return this.http.get<T>( query, { headers });      
    }

    getTopHeadlines(){
      return this.ejecutarQuery<RespuestaTopHeadlines>('/top-headlines?country=us');
      return this.http.get<RespuestaTopHeadlines>('http://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=8e8a72b30763490cb14ce3fc936e8105');
    }

    getTopHeadlinesEventos( eventos: string){
      return this.ejecutarQuery<RespuestaTopHeadlines>('/top-headlines?sources=techcrunch=${ categoria }')
      //return this.http.get('http://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=8e8a72b30763490cb14ce3fc936e8105');
    }
}
