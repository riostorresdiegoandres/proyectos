import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { Article } from '../interfaces/interfaces';
import { NoticiasService } from '../services/noticias.service';

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.page.html',
  styleUrls: ['./eventos.page.scss'],
})
export class EventosPage implements OnInit {

  @ViewChild(IonSegment) segment: IonSegment;
  categorias = ['academicos', 'deportivos', 'culturales'];
  noticias: Article[] = [];

  constructor( private noticiasService: NoticiasService ) { 

  }

  ngOnInit() {
    //this.segment.value = this.categorias[0];
   // this.noticiasService.getTopHeadlinesEventos( this.categorias[0] )
     //     .subscribe( resp => {
     //       console.log(resp);
      //      this.noticias.push( ...resp.articles);
     //     });
  }

}
