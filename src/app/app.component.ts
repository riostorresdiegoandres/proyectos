import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router'

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Inicio',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Noticias',
      url: '/noticias',
      icon: 'newspaper'
    },
    {
      title: 'Eventos',
      url: '/eventos',
      icon: 'list'
    },
    {
      title: 'Noticias UDES',
      url: '/noticiasnuevas',
      icon: 'newspaper'
    },
    {
      title: 'Eventos UDES',
      url: '/eventosnuevos',
      icon: 'list'
    },
    {
      title: 'Cuenta',
      url: '/cuenta',
      icon: 'Person'
    }
    
    
  ];
  

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router:Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.router.navigateByUrl('inicio')
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];

  }
}
