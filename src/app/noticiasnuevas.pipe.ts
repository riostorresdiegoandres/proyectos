import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noticiasnuevas'
})
export class NoticiasnuevasPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    return null;
  }

}
