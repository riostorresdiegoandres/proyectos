import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  constructor(
    private routes:Router,
    public menu: MenuController) { }

  ngOnInit() {

    this.menu.enable(false, "custom");

    setTimeout(() => {
      this.routes.navigateByUrl('login');
    }, 5000);

  }

}
