import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EventosnuevosPage } from './eventosnuevos.page';

describe('EventosnuevosPage', () => {
  let component: EventosnuevosPage;
  let fixture: ComponentFixture<EventosnuevosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventosnuevosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EventosnuevosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
