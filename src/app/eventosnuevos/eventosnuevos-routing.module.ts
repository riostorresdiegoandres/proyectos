import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EventosnuevosPage } from './eventosnuevos.page';

const routes: Routes = [
  {
    path: '',
    component: EventosnuevosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EventosnuevosPageRoutingModule {}
