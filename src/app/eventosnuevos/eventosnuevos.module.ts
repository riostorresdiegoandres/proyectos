import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EventosnuevosPageRoutingModule } from './eventosnuevos-routing.module';

import { EventosnuevosPage } from './eventosnuevos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EventosnuevosPageRoutingModule
  ],
  declarations: [EventosnuevosPage]
})
export class EventosnuevosPageModule {}
